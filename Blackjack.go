package main

import (
	"bufio"
	"fmt"
	"log"
	"math/rand"
	"os"
	"strconv"
)

// suits: clubs (♣), diamonds (♦), hearts (♥) and spades (♠)
// ranks: Ace, King, Queen, Jack, 10, 9, 8, 7, 6, 5, 4, 3, 2

var suits = [4]string{"clubs", "diamonds", "hearts", "spades"}
var ranks = [13]string{"Ace", "King", "Queen", "Jack", "10", "9", "8", "7", "6", "5", "4", "3", "2"}

type Card struct {
	suit string
	rank string
}

func (TheCard Card) PrintCardValue() int {

	var CardRank string = TheCard.rank

	switch CardRank {
	case "Ace":
		return 11
	case "King", "Queen", "Jack":
		return 10
	default:
		i, _ := strconv.Atoi(CardRank)
		return i
	}
}

func (TheCard Card) PrintCard() string {
	var CardString string = TheCard.rank + " of " + TheCard.suit
	return CardString
}

func GenerateDeck() []Card {
	var NewDeck = []Card{}

	for i := 0; i < len(suits); i++ {
		for m := 0; m < len(ranks); m++ {
			var TempCard Card
			TempCard.rank = ranks[m]
			TempCard.suit = suits[i]
			NewDeck = append(NewDeck, TempCard)
		}
	}

	return NewDeck
}

func ShuffleDeck(IngressDeck []Card) []Card {

	var EgressDeck []Card

	if len(IngressDeck) == 0 {
		panic("You gave me an empty deck to shuffle you bastard!")
	} else {
		var DeckSize int = len(IngressDeck)
		var ShuffleSeed []int = rand.Perm(52)

		for i := 0; i < DeckSize; i++ {
			var RandomNumber = ShuffleSeed[i]
			EgressDeck = append(EgressDeck, IngressDeck[RandomNumber])
		}
	}

	return EgressDeck
}

func DrawCard(TheDeck []Card, CardNum int) Card {
	return TheDeck[CardNum]
}

func PrintHand(TheHand []Card, TheMessage string, TheDealerFlag bool, HideScore bool) {

	var CurrentScore string = strconv.Itoa(GetHandScore(TheHand))
	var FullMessage string

	if HideScore {
		FullMessage = "\n" + TheMessage + "(?): "
	} else {
		FullMessage = "\n" + TheMessage + "(" + CurrentScore + "): "
	}

	fmt.Println(FullMessage)

	for n := 0; n < len(TheHand); n++ {
		if n == 0 && TheDealerFlag {
			fmt.Println("  - ?")
		} else {
			fmt.Println("  - " + TheHand[n].PrintCard())
		}
	}
}

func GetHandScore(TheHand []Card) int {
	var TheScore int = 0

	for i := 0; i < len(TheHand); i++ {
		TheScore += TheHand[i].PrintCardValue()
	}

	return TheScore
}

func FinalizeDealer(TheDealerHand, TheDeck []Card, TheCardIndex int) {
	var CurrentDealerHand = GetHandScore(TheDealerHand)

	if CurrentDealerHand <= 16 {
		TheDealerHand = append(TheDealerHand, TheDeck[TheCardIndex])
	}
}

func FindWinner(ThePlayerHand, TheDealerHand []Card) {

	PrintHand(TheDealerHand, "Final Dealer's hand", false, false)
	PrintHand(ThePlayerHand, "Final Player's hand", false, false)

	var PlayerScore int = GetHandScore(ThePlayerHand)
	var DealerScore int = GetHandScore(TheDealerHand)

	if PlayerScore > 21 {
		fmt.Println("\nBusted! Player lost!")
	} else if DealerScore > 21 {
		fmt.Println("\nPlayer won!")
	} else if DealerScore == 21 {
		if PlayerScore == 21 {
			fmt.Println("\nBlackjack is on your side! Player won!")
		} else {
			fmt.Println("\nDealer won!")
		}
	} else if PlayerScore == DealerScore {
		fmt.Println("Standoff. No one lost! No one won!")
	} else if PlayerScore > DealerScore {
		if PlayerScore == 21 {
			fmt.Println("\nBlackjack! Player won!")
		} else {
			fmt.Println("\nPlayer won!")
		}
	} else {
		fmt.Println("\nDealer won!")
	}

}

func main() {

	var Deck []Card = GenerateDeck()
	var ShuffledDeck []Card = ShuffleDeck(Deck)

	var PlayerHand []Card
	var DealerHand []Card

	for i := 0; i < len(ShuffledDeck); i++ {

		PlayerHand = append(PlayerHand, DrawCard(ShuffledDeck, i))
		i++

		var CurrentPlayerHand int = GetHandScore(PlayerHand)

		PrintHand(DealerHand, "\nCurrent Dealer's hand", true, true)
		PrintHand(PlayerHand, "\nCurrent Player's hand", false, false)

		if CurrentPlayerHand < 21 {

			DealerHand = append(DealerHand, DrawCard(ShuffledDeck, i))

			fmt.Print("\n(H)itting or (S)taying? ")

			reader := bufio.NewReader(os.Stdin)
			char, _, err := reader.ReadRune()

			if err != nil {
				log.Fatal(err)
			}

			if char == 'h' || char == 'H' {
				continue
			} else if char == 's' || char == 'S' {
				FinalizeDealer(DealerHand, ShuffledDeck, i)
				break
			} else {
				panic("Was this an option? WAS IT?")
			}
		} else {
			break
		}

	}

	FindWinner(PlayerHand, DealerHand)

}
